#!/usr/bin/env python

import socket, time, subprocess 

UDP_TARGET_IP = "192.100.1.10"
UDP_TARGET_PORT = 5005
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

while 1:
	temperature = subprocess.check_output("/opt/vc/bin/vcgencmd measure_temp", shell=True)
	sock.sendto(temperature[5:9], (UDP_TARGET_IP, UDP_TARGET_PORT))
	time.sleep(0.5)
