# Raspberry Pi Usage Strategy

This document, in brief explains the methodology followed.

**Pretext**
We choose to use Raspberry Pi3 as our main robot controller brain.

The reason for this choice were numerous, some of them :

- It is fast and not very expensive
- It has inbuild Wifi
- It can do encoding on hardware if camera module is used.

So our strategy was simple.

1. Use Raspberry Pi 3 as Access Point using hostapd.
2. Run video server using uv4l on Raspberry.
3. Connect Android Control device.
4. Open http connection to stream h.264 encoded video from raspberry.
5. Open UDP Sockets for control actions.
6. Manage all these above points by making a bash script which would automatically run on startup.


### References
1. [Run scripts on startup](http://www.stuffaboutcode.com/2012/06/raspberry-pi-run-program-at-start-up.html)
2. [Make Raspberry an Access Point](https://frillip.com/using-your-raspberry-pi-3-as-a-wifi-access-point-with-hostapd/)
3. [Run Video Server on Raspberry](https://www.linux-projects.org/uv4l/installation/)