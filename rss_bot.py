#!/usr/bin/env python
import socket, time
from gpiozero import Motor,  AngularServo
from subprocess import call
#call(["/opt/vc/bin/vcgencmd", "measure_temp"])
port = 12345 
current_angle = 0

motor1 = Motor(17,18)
motor2 = Motor(22,23)

servo = AngularServo(27,initial_angle=0, min_angle=180, max_angle=0)
time.sleep(0.2)
servo.value = None

def go_forward():
    motor1.forward()
    motor2.forward()

def go_back():
    motor1.backward()
    motor2.backward()
    
def go_left():
    motor1.forward()
    motor2.backward()
    
def go_right():
    motor2.forward()
    motor1.backward()

def cam_up(): 
    global current_angle
    if current_angle <= 160:
	current_angle +=20
	servo.angle = current_angle
	time.sleep(0.1)
    	servo.value = None
  
def cam_down():
    global current_angle
    if current_angle >= 20:
        current_angle -=20
	servo.angle = current_angle
	time.sleep(0.1)
        servo.value = None
   
def stop_now():
    motor1.stop()
    motor2.stop()
    
def halt_bot():
    call(["sudo", "shutdown", "-h", "now"])

rss_bot_commands = {
        'f' : go_forward,
        'b' : go_back,
        'r' : go_right,
        'l' : go_left,
        's' : stop_now,
	'u' : cam_up,
	'd' : cam_down,
	'h' : halt_bot,
}
        
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind(("", port))

while 1:
    data, addr = s.recvfrom(1)
    rss_bot_commands[data]()
                  
