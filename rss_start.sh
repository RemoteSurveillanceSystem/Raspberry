#! /bin/sh

### BEGIN INIT INFO
# Provides:          rss_start.sh
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Simple script to start a RSS at boot
# Description:    RSS Bot control script written in python
### END INIT INFO

sudo python /home/pi/rss/rss_bot.py &
sudo python /home/pi/rss/rss_temp.py &
